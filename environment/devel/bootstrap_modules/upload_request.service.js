(function() {
	'use strict';

	/**
	 * Monica Modena
	 * The service DcyUploadRequestService manages the generic upload request.
	 *
	 * @params elementUID  {string} <owner_type_id>
	 * @params resourceDest {string} destination resource path
	 *
	 */
	function UploadRequestService(dcyUtilsFactory, dcyEnvProvider) {

		var self = this;

		function getUploadInfo(additionalCtx) {

			var domainUrl = dcyEnvProvider.getItem('domainUrl', null, 'applicationInfo');

			var config = {
					serviceName: domainUrl,
					subPath: window.resourceURL('/UploadServlet'),
			};
			
			if(additionalCtx){
				config.params = {
					rasterCtx : additionalCtx
				};
			}

			return config;

		};

		self.getUploadInfo = getUploadInfo;
		
	}

	UploadRequestService.$inject = ['dcyUtilsFactory', 'dcyEnvProvider'];

	angular
	.module('dcyApp.services')
	.service('dcyUploadRequestService', UploadRequestService);

}());
