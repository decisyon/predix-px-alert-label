(function() {
	'use strict';

	/*
	 * Controller to manage the alertLabel widget.
	 * link: https://www.predix-ui.com/?show=px-alert-label&type=component
	 *
	*/
	function PxAlertLabelCtrl($scope, $timeout, $element) {
		var ctrl = this,
			alertLabel = $('px-alert-label', $element),
			ctx = $scope.DECISYON.target.content.ctx,
			refObjId = ctx.refObjId.value,
			BLANK_PARAM_VALUE = '',
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND';
	
		/* Management of label attribute */
		var manageLabelAttr = function(context) {
			var alertLabel = $('<div/>').text(context.$pxalertlabel.value).html();
			ctrl.alertLabelData = (angular.equals(alertLabel, EMPTY_PARAM_VALUE)) ? BLANK_PARAM_VALUE : alertLabel;
		};

		/* Management of type attribute */
		var manageTypeAttr = function(context) {
			ctrl.alertType = context.$pxalerttype.value.value;
		};

		/* Managing of the custom settings from context */
		var manageParamsFromContext = function(context) {
			manageLabelAttr(context);
			manageTypeAttr(context);
		};

		/* Watch on widget context */
		var listenerOnDataChange = function() {
			$scope.$watch('DECISYON.target.content.ctx', function(newContent, oldContent) {
				if (!angular.equals(newContent, oldContent)) {
					manageParamsFromContext(newContent);
				}
			}, true);
		};

		/* Initialize */
		var inizialize = function() {
			ctrl.widgetID = 'pxAlertLabel_' + refObjId;
			manageParamsFromContext(ctx);
			listenerOnDataChange();
		};
		
		/* When the predix ui widget was completed. Callback called when dcyOnLoad directive has completed. */
		ctrl.whenPredixalertLabelLibraryLoaded = function() {
			if (alertLabel) {
				inizialize();
			}
		};
	
	}
	PxAlertLabelCtrl.$inject = ['$scope', '$timeout', '$element'];
	DECISYON.ng.register.controller('pxAlertLabelPredixCtrl', PxAlertLabelCtrl);

}());